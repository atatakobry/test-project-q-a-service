(function(){

    'use strict';

    angular.module('qa').directive('menu', function() {
        return {
            restrict: 'E',
            templateUrl: '/common/directives/menu/menu.html',
            scope: '='
        };
    }).directive('activeLink', function($rootScope, $timeout) {
        return {
            restrict: 'A',
            link: function(scope, el, attrs){
                $timeout(function(){
                    $rootScope.$watch('currentState.url', function(newValue){
                        newValue == attrs.url ? el.parent().addClass('active'): el.parent().removeClass('active');
                    });
                }, 10)
            }
        };
    });

}());