(function(){

    'use strict';

    angular.module('qa.question').controller('questionController', ['$rootScope', '$scope', '$state', '$filter', '$http', function ($rootScope, $scope, $state, $filter, $http) {
        $scope.questionId = +$state.params.id;

        $http.get('/api/question/' + $scope.questionId)
            .then(function(response){
                $scope.question = angular.copy(response.data);    
                $scope.questionLength = $filter('keylength')($scope.question.answers); 
            });

        $scope.data = {};
        $scope.data.author = $rootScope.user.name;

        $scope.postNewAnswer = function(data) {
            $http.post('/api/question/' + $scope.questionId + '/new-answer', angular.toJson(data))
                .then(function(response){
                    $state.reload();
                    // console.log(response);          
                });
        };
    }]);

}());