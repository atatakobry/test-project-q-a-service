(function(){

    'use strict';

    angular.module('qa.home').controller('homeController', ['$scope', '$http', function ($scope, $http) {
    	$scope.questions = {};
        $scope.getQuestions = function(mode) {
            $http.get('/api/questions/' + mode)
                .then(function(response){
                    $scope.questions = response.data;               
                });
        };

    	$scope.questionFilterRadio = '0';
    	$scope.getQuestions('all');
    }]);

}());