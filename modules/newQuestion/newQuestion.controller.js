(function(){

    'use strict';

    angular.module('qa.newQuestion').controller('newQuestionController', ['$scope', '$rootScope', '$http', '$state', function ($scope, $rootScope, $http, $state) {
        $scope.data = {};
        $scope.data.author = $rootScope.user.name;
        $scope.data.answers = [];

        $scope.postNewQuestion = function(data) {
            $http.post('/api/new-question', angular.toJson(data))
                .then(function(response){
                	$state.go('index');
                    // console.log(response);          
                });
        };
    }]);

}());