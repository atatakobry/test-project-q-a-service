(function(){

    'use strict';

    angular.module('qa', [
        'ngRoute',
        'ui.router',
        'ui.bootstrap',

        'qa.home',
        'qa.question',
        'qa.newQuestion'
    ])
    .controller('qaController', ['$rootScope', '$scope', '$location', '$state', '$timeout', '$http', function($rootScope, $scope, $location, $state, $timeout, $http){
        $rootScope.$on('$stateChangeSuccess', function(event, to, toParams, from, fromParams) {
            $rootScope.loginPage =  to.name == 'login';
            $rootScope.$previousState = from;
            $rootScope.currentState = to;
            angular.forEach(fromParams, function(val, key){
                $rootScope.$previousState.url = $rootScope.$previousState.url.replace(key, key + '=' + val);
            });
        });

        $rootScope.authorized = false;
        $rootScope.user = {
            "name": ""
        }; 
    }])
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .filter('keylength', function() {
        return function(input) {
            if(!angular.isObject(input)) {
                return 0;
            }
            return Object.keys(input).length;
        }
    });
}());