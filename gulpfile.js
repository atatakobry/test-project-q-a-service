var gulp          = require('gulp'),
    connect       = require('gulp-connect'),
    sass          = require('gulp-sass'),
    concat        = require('gulp-concat'),
    prefix        = require('gulp-autoprefixer'),
    minifyCSS     = require('gulp-minify-css'),
    autowatch     = require('gulp-autowatch');

// SCSS

gulp.task('base-styles', function() {
  return gulp.src('assets/scss/base/*.scss')
    .pipe(concat('base.scss'))
    .pipe(sass())
    .pipe(prefix(["last 5 version", "ie 9",]))
    .pipe(minifyCSS({keepBreaks: true}))
    .pipe(gulp.dest('assets/css'));
});

gulp.task('styles', function() {
  return gulp.src('assets/scss/*.scss')
    .pipe(concat('style.scss'))
    .pipe(sass())
    .pipe(prefix(["last 5 version", "ie 9"]))
    .pipe(minifyCSS({keepBreaks: true}))
    .pipe(gulp.dest('assets/css'));
});

// WATCH

var paths = {
  'base-styles':  'assets/scss/base/*.scss',
  styles:         'assets/scss/*.scss'
};

gulp.task('watch', function(cb) {
  autowatch(gulp, paths);
  return cb();
});

// CONNECT 

// gulp.task('connect', function() {
//   connect.server({
//     port: '3002',
//     livereload: false
//   });
// });


// DEFAULT

gulp.task('default',  [
  // 'connect',
  'base-styles',
  'styles',
  'watch'
]);
