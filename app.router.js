(function(){

    'use strict';

    angular.module('qa').config(['$routeProvider', '$locationProvider', '$stateProvider', '$urlRouterProvider', '$urlMatcherFactoryProvider',
        function($routeProvider, $locationProvider, $stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider) {
            $locationProvider.html5Mode(true);

            $urlRouterProvider.otherwise('/');

            $stateProvider.state('index', {
                url: '/',
                templateUrl: 'modules/home/home.html',
                controller: 'homeController'
            }).state('question', {
                url: '/question/:id',
                templateUrl: 'modules/question/question.html',
                controller: 'questionController'
            }).state('newQuestion', {
                url: '/new-question',
                templateUrl: 'modules/newQuestion/newQuestion.html',
                controller: 'newQuestionController'
            });
        }]);
}());
