var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.use("/", express.static(__dirname + "/"));

var server = app.listen(3000, function () {
  	var host = server.address().address;
	var port = server.address().port;
});

app.get('/', function (req, res) {
	res.sendfile('./index.html');
});

// api

app.get('/api/questions/all', function (req, res) {
	res.send(questions);
});

app.get('/api/questions/without', function (req, res) {
	res.send(getQuestionsWithoutAnswers(questions));
});

app.get('/api/questions/with', function (req, res) {
	res.send(getQuestionsWithAnswers(questions));
});

app.get('/api/question/:id', function (req, res) {
	res.send(questions[req.params.id]);
});

app.post('/api/question/:id/new-answer', function (req, res) {
	res.send('ok');
	var answer = req.body;
	questions[req.params.id].answers.push(answer);
});

app.post('/api/new-question', function (req, res) {
	res.send('ok');
	var question = req.body;
	question.id = questionsLength++;
	questions.push(question);
});

// DB SIMULATION

var questions =
[
    {
        "id": "0",
        "title": "Как мне получить эту работу?",
        "description": "...",
        "author": "atatakobry",
        "answers": []
    },
    {
        "id": "1",
        "title": "Как пропатчить kde2 под freebsd?",
        "description": "Добрый день, спрашивал этот вопрос в аниме-группах в контактике, так и не нашел ответа Т_Т. ПОМОГИТЕ",
        "author": "admin",
        "answers": [
            {
                "author": "otaku",
                "message": "KAWAII ＼(＾▽＾)／ !!!!"
            }
        ]
    },
    {
        "id": "2",
        "title": "Что будет, если в унитаз поезда на полном ходу бросить лом?",
        "description": "Собственно, сабж.",
        "author": "lurkmore",
        "answers": [
            {
                "author": "user1",
                "message": "Лом согнется."
            },
            {
                "author": "user2",
                "message": "Лом выскочит назад."
            },
            {
                "author": "user3",
                "message": "Лом прокопает землю вместе со шпалами."
            },
            {
                "author": "user3",
                "message": "Поезд сойдёт с рельс."
            }
        ]
    },
    {
        "id": "3",
        "title": "А вы тоже...",
        "description": "брали авторучку, нажимали на кнопку, ставили её на стол и она взлетала как ракета?",
        "author": "Аноним",
        "answers": []
    },
    {
        "id": "4",
        "title": "Зачем зайцу стоп сигнал?",
        "description": "и козе боян?",
        "author": "Волк",
        "answers": []
    }
];

var questionsLength = Object.keys(questions).length;

function getQuestionsWithoutAnswers(questions) {
	var l = questions.length;
	var results = [];

	for(var i = 0; i < l; i++) {
		if(!questions[i].answers.length) {
			results.push(questions[i]);
		}
	}

	return results;
}

function getQuestionsWithAnswers(questions) {
	var l = questions.length;
	var results = [];

	for(var i = 0; i < l; i++) {
		if(questions[i].answers.length) {
			results.push(questions[i]);
		}
	}

	return results;
}




